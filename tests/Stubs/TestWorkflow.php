<?php

namespace Tests\Stubs;

use Fluick\Flow\Definition;
use Fluick\Flow\Step;
use Fluick\Flow\Transition;
use Fluick\Flow\Workflow;

class TestWorkflow implements Workflow
{
    /** @var string */
    protected $name = 'test';

    /** @var string */
    protected $description = 'Test Workflow';

    public function definition(): Definition
    {
        $definition = new Definition($this->name, $this->description);

        $createdStep = new Step('created');
        $editedStep = new Step('edited');
        $deletedStep = new Step('deleted');

        $createTransition = new Transition('create', $definition, $createdStep);
        $editTransition = new Transition('edit', $definition, $editedStep);

        $createdStep
            ->allowTransition($createTransition)
            ->allowTransition($editTransition);

        return $definition
            ->addStep($createdStep)
            ->addStep($editedStep)
            ->addStep($deletedStep);
    }
}