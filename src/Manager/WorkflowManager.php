<?php

namespace Fluick\Manager;

use Fluick\Flow\Workflow;
use Fluick\Handler\WorkflowHandler;
use Fluick\Process\Process;
use Fluick\Process\WorkflowProcess;
use InvalidArgumentException;

/**
 * Class WorkflowManager
 *
 * @package Fluick\Manager
 */
class WorkflowManager implements Manager
{

    public function handle(Workflow $workflow, WorkflowHandler $handler): Process
    {
        $definition = $workflow->definition();

        if ($definition) {
            return new WorkflowProcess($definition, $handler);
        }

        throw new InvalidArgumentException("Workflow definition is not null");
    }
}