<?php

namespace Fluick\Manager;

use Fluick\Flow\Workflow;
use Fluick\Handler\WorkflowHandler;
use Fluick\Process\Process;

/**
 * Interface Manager
 *
 * @package Fluick\Manager
 */
interface Manager
{
    /**
     * Handle workflow definition
     *
     * @param Workflow $workflow
     * @param WorkflowHandler $handler
     * @return Process
     */
    public function handle(Workflow $workflow, WorkflowHandler $handler): Process;
}