<?php

namespace Fluick\Repository;

use Fluick\Flow\Item;

/**
 * Interface EntityRepository describes the repository which stores the items.
 *
 * @package Fluick\Repository
 */
interface EntityRepository
{
    /**
     * @param Item $item
     */
    public function update(Item $item): void;
}