<?php

namespace Fluick\Repository;

use Fluick\Flow\State;

/**
 * Interface StateRepository stores workflow states
 *
 * @package Fluick\Data
 */
interface StateRepository
{
    /**
     * Find last workflow state of an entity.
     *
     * @param int $entityId The entity id.
     * @return State[]
     */
    public function find(int $entityId): array;

    /**
     * Add a new state.
     *
     * @param State $state The new state.
     *
     * @return void
     */
    public function add(State $state): void;
}