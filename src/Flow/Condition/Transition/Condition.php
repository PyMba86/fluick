<?php

namespace Fluick\Flow\Condition\Transition;

use Fluick\Flow\Context;
use Fluick\Flow\Item;
use Fluick\Flow\Transition;

/**
 * Interface Condition describes an condition which used for transition conditions.
 *
 * @package Fluick\Flow\Condition\Transition
 */
interface Condition
{
    /**
     * Consider if condition matches for the given entity.
     *
     * @param Transition $transition The transition being in.
     * @param Item $item The entity being transits.
     * @param Context $context The transition context.
     *
     * @return bool
     */
    public function match(Transition $transition, Item $item, Context $context): bool;
}