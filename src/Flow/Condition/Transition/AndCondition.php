<?php

namespace Fluick\Flow\Condition\Transition;

use Fluick\Flow\Context;
use Fluick\Flow\Item;
use Fluick\Flow\Transition;

/**
 * Class AndCondition matches if all child conditions does.
 *
 * @package Fluick\Flow\Condition\Transition
 */
class AndCondition extends ConditionCollection
{

    public function match(Transition $transition, Item $item, Context $context): bool
    {
        $localContext = $context->createCleanCopy();
        $success = true;

        foreach ($this->conditions as $condition) {
            if (!$condition->match($transition, $item, $localContext)) {
                $success = false;
            }
        }

        if (!$success) {
            $context->addError(
                'transition.condition.and.failed',
                [],
                $localContext->getErrorCollection()
            );
        }

        return $success;
    }
}