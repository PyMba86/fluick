<?php

namespace Fluick\Flow\Condition\Workflow;

use Fluick\Flow\Definition;

class AndCondition extends ConditionCollection
{
    /**
     * {@inheritdoc}
     */
    public function match(Definition $definition, int $entityId, array $entity): bool
    {
        foreach ($this->conditions as $condition) {
            if (!$condition->match($definition, $entityId, $entity)) {
                return false;
            }
        }

        return true;
    }
}
