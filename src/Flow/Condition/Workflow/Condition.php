<?php

namespace Fluick\Flow\Condition\Workflow;

use Fluick\Flow\Definition;

interface Condition
{
    /**
     * Consider if workflow matches to the entity.
     *
     * @param Definition $definition The current workflow.
     * @param int $entityId The entity id.
     * @param mixed $entity The entity.
     *
     * @return bool
     */
    public function match(Definition $definition, int $entityId, array $entity): bool;
}