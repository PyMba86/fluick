<?php

namespace Fluick\Flow\Context;

/**
 * Class Properties
 *
 * @package Fluick\Flow\Context
 */
class Properties
{
    /**
     * Properties.
     *
     * @var array
     */
    private $properties;

    /**
     * Properties constructor.
     *
     * @param array $properties Properties.
     */
    public function __construct(array $properties = [])
    {
        $this->properties = $properties;
    }

    /**
     * Get properties.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->properties;
    }

    /**
     * Check if a property exists.
     *
     * @param string $propertyName Name of the property.
     *
     * @return bool
     */
    public function has(string $propertyName): bool
    {
        return array_key_exists($propertyName, $this->properties);
    }

    /**
     * Set a property value.
     *
     * @param string $propertyName Name of the property.
     * @param mixed  $value        Value of the property.
     *
     * @return Properties
     */
    public function set(string $propertyName, $value): self
    {
        $this->properties[$propertyName] = $value;

        return $this;
    }

    /**
     * Get the property value. If property does not exist, null is returned.
     *
     * @param string $propertyName Name of the property.
     *
     * @return mixed
     */
    public function get(string $propertyName)
    {
        if (isset($this->properties[$propertyName])) {
            return $this->properties[$propertyName];
        }

        return null;
    }
}