<?php

namespace Fluick\Flow;

/**
 * Interface Action describes an action which is executed during transition.
 *
 * @package Fluick\Flow
 */
interface Action
{
    /**
     * Get the required payload properties.
     *
     * @param Item $item Workflow item.
     *
     * @return array
     */
    public function getRequiredPayloadProperties(Item $item): array;

    /**
     * Validate the given item and context (payload properties).
     *
     * @param Item $item Workflow item.
     * @param Context $context Transition context.
     *
     * @return bool
     */
    public function validate(Item $item, Context $context): bool;

    /**
     * Transit will execute the action.
     *
     * @param Transition $transition Current transition.
     * @param Item $item Workflow item.
     * @param Context $context Transition context.
     *
     * @return void
     */
    public function transit(Transition $transition, Item $item, Context $context): void;
}