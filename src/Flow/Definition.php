<?php

namespace Fluick\Flow;

use Fluick\Flow\Condition\Workflow\AndCondition;
use Fluick\Flow\Condition\Workflow\Condition;
use Fluick\Flow\Exception\StepNotFoundException;
use Fluick\Flow\Exception\TransitionNotFound;

/**
 * Class Workflow stores all information of a step processing workflow.
 *
 * @package Fluick\Flow
 */
class Definition extends Element
{
    /**
     * Steps being available in the workflow
     *
     * @var Step[]
     */
    private $steps = [];

    /**
     * Transitions being available in the workflow.
     *
     * @var Transition[]
     */
    private $transitions = [];

    /**
     * The start transition.
     *
     * @var Transition
     */
    private $startTransition;

    /**
     * Condition to supports if workflow can handle an entity.
     *
     * @var AndCondition
     */
    private $condition;

    public function addStep(Step $step): self
    {
        $this->steps[] = $step;

        return $this;
    }

    /**
     * Add a transition to the workflow.
     *
     * @param Transition $transition Transition to be added.
     * @param bool $startTransition True if transition will be the start transition.
     *
     * @return $this
     */
    public function addTransition(Transition $transition, $startTransition = false): self
    {
        if (in_array($transition, $this->transitions)) {
            return $this;
        }

        $this->transitions[] = $transition;

        if ($startTransition) {
            $this->startTransition = $transition;
        }

        return $this;
    }

    /**
     * Get a transition by name.
     *
     * @param string $transitionName The name of the transition.
     *
     * @return Transition
     * @throws TransitionNotFound If transition is not found.
     *
     */
    public function getTransition(string $transitionName): Transition
    {
        foreach ($this->transitions as $transition) {
            if ($transition->getName() == $transitionName) {
                return $transition;
            }
        }

        throw TransitionNotFound::withName($transitionName, $this->getName());
    }

    /**
     * Get allowed transitions for a workflow item.
     *
     * @param Item $item Workflow item.
     * @param Context|null $context Transition context.
     *
     * @return Transition[]|iterable
     */
    public function getAvailableTransitions(Item $item, Context $context = null): iterable
    {
        if ($context) {
            $context = $context->createCleanCopy();
        } else {
            $context = new Context();
        }

        if (!$item->isWorkflowStarted() || $item->getWorkflowName() !== $this->getName()) {
            $transitions = array($this->getStartTransition());
        } else {
            $step = $this->getStep($item->getCurrentStepName());
            $transitions = array_map(
                function ($transitionName) {
                    return $this->getTransition($transitionName);
                },
                $step->getAllowedTransitions()
            );
        }

        return array_values(
            array_filter(
                $transitions,
                function (Transition $transition) use ($item, $context) {
                    return $transition->isAvailable($item, $context);
                }
            )
        );
    }

    /**
     * Get all transitions.
     *
     * @return Transition[]|iterable
     */
    public function getTransitions(): iterable
    {
        return $this->transitions;
    }

    /**
     * Check if transition is part of the workflow.
     *
     * @param string $transitionName Transition name.
     *
     * @return bool
     */
    public function hasTransition(string $transitionName): bool
    {
        foreach ($this->transitions as $transition) {
            if ($transition->getName() === $transitionName) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if a specific transition is available.
     *
     * @param Item $item The workflow item.
     * @param Context $context Transition context.
     * @param string $transitionName The transition name.
     *
     * @return bool
     */
    public function isTransitionAvailable(
        Item $item,
        Context $context,
        string $transitionName
    ): bool
    {
        if (!$item->isWorkflowStarted()) {
            return $this->getStartTransition()->getName() === $transitionName;
        }

        $step = $this->getStep($item->getCurrentStepName());
        if (!$step->isTransitionAllowed($transitionName)) {
            return false;
        }

        $transition = $this->getTransition($transitionName);

        return $transition->isAvailable($item, $context);
    }

    /**
     * Get a step by step name.
     *
     * @param string $stepName The step name.
     *
     * @return Step
     *
     * @throws StepNotFoundException If step is not found.
     */
    public function getStep(string $stepName): Step
    {
        foreach ($this->steps as $step) {
            if ($step->getName() == $stepName) {
                return $step;
            }
        }

        throw new StepNotFoundException($stepName, $this->getName());
    }

    /**
     * Check if step with a name exist.
     *
     * @param string $stepName The step name.
     *
     * @return bool
     */
    public function hasStep(string $stepName): bool
    {
        foreach ($this->steps as $step) {
            if ($step->getName() == $stepName) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set transition as start transition.
     *
     * @param string $transitionName Name of start transition.
     *
     * @return $this
     * @throws TransitionNotFound If transition is not part of the workflow.
     *
     */
    public function setStartTransition(string $transitionName): self
    {
        $this->startTransition = $this->getTransition($transitionName);

        return $this;
    }

    /**
     * Get the start transition.
     *
     * @return Transition
     */
    public function getStartTransition(): Transition
    {
        return $this->startTransition;
    }

    /**
     * Get the current condition.
     *
     * @return AndCondition
     */
    public function getCondition(): ?AndCondition
    {
        return $this->condition;
    }

    /**
     * Shortcut to add a condition to the condition collection.
     *
     * @param Condition $condition Condition to be added.
     *
     * @return $this
     */
    public function addCondition(Condition $condition): Definition
    {
        if (!$this->condition) {
            $this->condition = new AndCondition();
        }

        $this->condition->addCondition($condition);

        return $this;
    }

    /**
     * Consider if workflow is supports an entity.
     *
     * @param int $entityId The entity id.
     * @param array $entity The entity.
     *
     * @return bool
     */
    public function supports(int $entityId, array $entity): bool
    {
        if (!$this->condition) {
            return true;
        }

        return $this->condition->match($this, $entityId, $entity);
    }

}