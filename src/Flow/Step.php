<?php

namespace Fluick\Flow;

/**
 * Class Step defines fixed step in the workflow process.
 *
 * @package Fluick\Flow
 */
class Step extends Element
{
    /**
     * The allowed transition names;
     *
     * @var string[]
     */
    private $allowedTransitions = [];

    /**
     * Step is a final step.
     *
     * @var bool
     */
    private $final = false;

    /**
     * The workflow name.
     *
     * For BC reasons it might be null.
     *
     * @var string|null
     */
    private $workflowName;

    /**
     * Construct.
     *
     * @param string $name Name of the element.
     * @param string $description Label of the element.
     * @param array $config Configuration values.
     * @param string|null $workflowName Name of the corresponding workflow. For BC reasons it might be null.
     */
    public function __construct(string $name, string $description = '',
                                array $config = [], ?string $workflowName = null)
    {
        parent::__construct($name, $description, $config);

        $this->workflowName = $workflowName;
    }

    /**
     * Consider if step is final.
     *
     * @return bool
     */
    public function isFinal(): bool
    {
        return $this->final;
    }

    /**
     * Mark step as final.
     *
     * @param bool $final Step is a final step.
     *
     * @return $this
     */
    public function setFinal(bool $final): self
    {
        $this->final = $final;

        return $this;
    }

    /**
     * Allow a transition.
     *
     * @param string $transitionName The name of the allowed transition.
     *
     * @return $this
     */
    public function allowTransition(string $transitionName): self
    {
        if (!in_array($transitionName, $this->allowedTransitions)) {
            $this->allowedTransitions[] = $transitionName;
        }

        return $this;
    }

    /**
     * Get workflow name.
     *
     * @return string|null
     */
    public function getWorkflowName(): ?string
    {
        return $this->workflowName;
    }

    /**
     * Disallow a transition.
     *
     * @param string $transitionName The name of the disallowed transition.
     *
     * @return $this
     */
    public function disallowTransition(string $transitionName): self
    {
        $key = array_search($transitionName, $this->allowedTransitions);

        if ($key !== false) {
            unset($this->allowedTransitions[$key]);
            $this->allowedTransitions = array_values($this->allowedTransitions);
        }

        return $this;
    }

    /**
     * Get all allowed transition names.
     *
     * @return array
     */
    public function getAllowedTransitions(): array
    {
        if ($this->isFinal()) {
            return [];
        }

        return $this->allowedTransitions;
    }

    /**
     * Consider if transition is allowed.
     *
     * @param string $transitionName The name of the checked transition.
     *
     * @return bool
     */
    public function isTransitionAllowed(string $transitionName): bool
    {
        if ($this->isFinal()) {
            return false;
        }

        return in_array($transitionName, $this->allowedTransitions);
    }
}