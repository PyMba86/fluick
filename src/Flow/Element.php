<?php

namespace Fluick\Flow;

/**
 * Class Configurable is the base class for each flow elements.
 *
 * @package Fluick\Flow
 */
abstract class Element
{
    /**
     * Configuration values
     *
     * @var array
     */
    private $config;

    /**
     * Name of the element.
     *
     * @var string
     */
    private $name;

    /**
     * Description of the element.
     *
     * @var string
     */
    private $description;

    /**
     * Construct.
     *
     * @param string $name Name of the element.
     * @param string $description Description of the element
     * @param array $config Configuration values.
     */
    public function __construct(string $name, string $description = '', array $config = [])
    {
        $this->name = $name;
        $this->description = $description ?: $name;
        $this->config = $config;
    }

    /**
     * Get element description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set the description.
     *
     * @param string $description The description.
     *
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get element name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set a config value.
     *
     * @param string $name Config property name.
     * @param mixed $value Config property value.
     *
     * @return $this
     */
    public function setConfigValue(string $name, $value): self
    {
        $this->config[$name] = $value;

        return $this;
    }

    /**
     * Get a config value.
     *
     * @param string $name Config property name.
     * @param mixed $default Default value which is returned if config is not set.
     *
     * @return mixed
     */
    public function getConfigValue(string $name, $default = null)
    {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }

        return $default;
    }

    /**
     * Consider if config value isset.
     *
     * @param string $name Name of the config value.
     *
     * @return bool
     */
    public function hasConfigValue(string $name): bool
    {
        return isset($this->config[$name]);
    }

    /**
     * Add multiple config properties.
     *
     * @param array $values Config values.
     *
     * @return $this
     */
    public function addConfig(array $values): self
    {
        foreach ($values as $name => $value) {
            $this->setConfigValue($name, $value);
        }

        return $this;
    }

    /**
     * Remove a config property.
     *
     * @param string $name Config property name.
     *
     * @return $this
     */
    public function removeConfigValue(string $name): self
    {
        unset($this->config[$name]);

        return $this;
    }

    /**
     * Get configuration.
     *
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }
}