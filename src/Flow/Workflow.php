<?php

namespace Fluick\Flow;

/**
 * Class Flow
 *
 * @package Fluick\Flow
 */
interface Workflow
{
    public function definition(): Definition;
}