<?php

namespace Fluick\Flow;

use Fluick\Flow\Context\ErrorCollection;
use Fluick\Flow\Context\Properties;

/**
 * Class Context provides extra information for a transition.
 *
 * @package Fluick\Flow
 */
class Context
{
    /**
     * Properties which will be stored as state data.
     *
     * @var Properties
     */
    private $properties;

    /**
     * Transition payload.
     *
     * @var array
     */
    private $payload;

    /**
     * Error collection.
     *
     * @var ErrorCollection
     */
    private $errorCollection;

    /**
     * Context constructor.
     * @param Properties|null $properties
     * @param array|null $payload
     * @param ErrorCollection|null $errorCollection
     */
    public function __construct(Properties $properties = null,
                                array $payload = null,
                                ErrorCollection $errorCollection = null)
    {
        $this->properties = $properties ?: new Properties();
        $this->payload = $payload;
        $this->errorCollection = $errorCollection ?: new ErrorCollection();
    }

    /**
     * Get properties.
     *
     * @return Properties
     */
    public function getProperties(): Properties
    {
        return $this->properties;
    }

    /**
     * Get payload.
     *
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * Get error collection.
     *
     * @return ErrorCollection
     */
    public function getErrorCollection(): ErrorCollection
    {
        return $this->errorCollection;
    }

    /**
     * Add an error.
     *
     * @param string $message Error message.
     * @param array $params Params for the error message.
     * @param ErrorCollection|null $collection Option. Child collection of the error.
     *
     * @return self
     */
    public function addError(string $message, array $params = array(), ErrorCollection $collection = null): self
    {
        $this->errorCollection->addError($message, $params, $collection);

        return $this;
    }

    /**
     * Get a new context with an empty error collection.
     *
     * @param array|null $payload Optional pass a new set of payload.
     *
     * @return Context
     */
    public function createCleanCopy(array $payload = null): Context
    {
        if ($payload === null) {
            $payload = $this->payload;
        }

        return new Context($this->properties, $payload, new ErrorCollection());
    }
}