<?php

namespace Fluick\Flow;

use Assert\Assertion;
use Fluick\Exception\FluickException;
use Fluick\Flow\Exception\FlowException;

/**
 * Class Item stores workflow related data of an entity. It knows the state history and the current state.
 *
 * @package Fluick\Flow
 */
class Item
{
    /**
     * Workflow name.
     *
     * @var string
     */
    private $workflowName;

    /**
     * Current step name.
     *
     * @var string
     */
    private $currentStepName;

    /**
     * State history which is already persisted.
     *
     * @var State[]
     */
    private $stateHistory = [];

    /**
     * Recorded state changes not persisted yet.
     *
     * @var State[]
     */
    private $recordedStateChanges = [];

    /**
     * Workflow entity.
     *
     * @var array
     */
    private $entity;

    /**
     * Entity id.
     *
     * @var int
     */
    private $entityId;

    /**
     * Construct. Do not used constructor. Use named constructor static methods.
     *
     * @param int $entityId The entity id.
     * @param array $entity The entity for which the workflow is started.
     */
    protected function __construct(int $entityId, array $entity)
    {
        $this->entityId = $entityId;
        $this->entity = $entity;
    }

    /**
     * Initialize a new workflow item.
     *
     * It is called before the workflow is started.
     *
     * @param int $entityId The entity id for the containing entity.
     * @param mixed $entity The entity for which the workflow is started.
     *
     * @return Item
     */
    public static function initialize(int $entityId, array $entity): self
    {
        return new Item($entityId, $entity);
    }

    /**
     * Restore an existing item.
     *
     * @param int $entityId The entity id.
     * @param mixed $entity The entity.
     * @param State[] $stateHistory Set or already passed states.
     *
     * @return Item
     */
    public static function reconstitute(int $entityId, array $entity, array $stateHistory): Item
    {
        Assertion::allIsInstanceOf($stateHistory, State::class);

        $item = self::initialize($entityId, $entity);

        // replay states
        foreach ($stateHistory as $state) {
            $item->apply($state);
        }

        return $item;
    }

    /**
     * Detach item from current workflow.
     *
     * You should only use it with care if the workflow has changed and there is no way to finish it.
     *
     * @return void
     */
    public function detach(): void
    {
        $this->currentStepName = null;
        $this->workflowName = null;
    }


    /**
     * Guard that workflow of item was not already started.
     *
     * @return void
     *
     * @throws FlowException If item workflow process was already started.
     */
    private function guardNotStarted(): void
    {
        if ($this->isWorkflowStarted()) {
            throw new FlowException('Item is already started.');
        }
    }

    /**
     * Guard that workflow of item is started.
     *
     * @return void
     *
     * @throws FlowException If item workflow process was not started.
     */
    private function guardStarted(): void
    {
        if (!$this->isWorkflowStarted()) {
            throw new FlowException('Item has not started yet.');
        }
    }

    /**
     * Start an item and return current state.
     *
     * @param Transition $transition The transition being executed.
     * @param Context $context The transition context.
     * @param bool $success The transition success.
     *
     * @return State
     *
     * @throws FluickException If workflow is already started.
     */
    public function start(
        Transition $transition,
        Context $context,
        bool $success
    ): State
    {
        $this->guardNotStarted();

        $state = State::start($this->entityId, $transition, $context, $success);
        $this->record($state);

        return $state;
    }

    /**
     * Transits to a new state and return it.
     *
     * @param Transition $transition The transition being executed.
     * @param Context $context The transition context.
     * @param bool $success The transition success.
     *
     * @return State
     * @throws FluickException If workflow is not started.
     *
     */
    public function transit(
        Transition $transition,
        Context $context,
        bool $success
    ): State
    {
        $this->guardStarted();

        $state = $this->getLatestStateOccurred();

        if ($state) {

            $state = $state->transit($transition, $context, $success);

            $this->record($state);
        }

        return $state;
    }

    /**
     * Release the recorded state changes.
     *
     * Reset the internal recorded state changes and return them.
     *
     * @return State[]
     */
    public function releaseRecordedStateChanges(): array
    {
        $recordedStates = $this->recordedStateChanges;
        $this->recordedStateChanges = [];

        return $recordedStates;
    }

    /**
     * Get the name of the current step.
     *
     * @return string
     */
    public function getCurrentStepName(): ?string
    {
        return $this->currentStepName;
    }

    /**
     * Get the entity id.
     *
     * @return int
     */
    public function getEntityId(): int
    {
        return $this->entityId;
    }

    /**
     * Get the entity.
     *
     * @return array
     */
    public function getEntity(): array
    {
        return $this->entity;
    }

    public function setEntity(array $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * Get the state history of the workflow item.
     *
     * @return State[]|iterable
     */
    public function getStateHistory(): array
    {
        return $this->stateHistory;
    }

    /**
     * Get latest state which occurred no matter if successful or not.
     *
     * @return State|null
     */
    public function getLatestStateOccurred(): ?State
    {
        if (count($this->stateHistory) === 0) {
            return null;
        }

        $index = (count($this->stateHistory) - 1);

        return $this->stateHistory[$index];
    }

    /**
     * Get latest successful state which occurred.
     *
     * @return State|null
     */
    public function getLatestSuccessfulState(): ?State
    {
        for ($index = (count($this->stateHistory) - 1); $index >= 0; $index--) {
            if ($this->stateHistory[$index]->isSuccessful()) {
                return $this->stateHistory[$index];
            }
        }

        return null;
    }

    /**
     * Get name of the workflow.
     *
     * @return string
     */
    public function getWorkflowName(): ?string
    {
        return $this->workflowName;
    }

    /**
     * Consider if workflow has started.
     *
     * @return bool
     */
    public function isWorkflowStarted(): bool
    {
        return !empty($this->currentStepName);
    }

    /**
     * Record a new state change.
     *
     * @param State $state The state being assigned.
     *
     * @return void
     */
    private function record(State $state): void
    {
        $this->recordedStateChanges[] = $state;
        $this->apply($state);
    }

    /**
     * Apply a new state.
     *
     * @param State $state The state being assigned.
     *
     * @return void
     */
    private function apply(State $state): void
    {
        // only change current step if transition was successful
        if ($state->isSuccessful()) {
            $this->currentStepName = $state->getStepName();
            $this->workflowName = $state->getWorkflowName();
        } elseif (!$this->isWorkflowStarted()) {
            $this->workflowName = $state->getWorkflowName();
        }

        $this->stateHistory[] = $state;
    }
}