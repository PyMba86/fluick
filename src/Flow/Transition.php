<?php

namespace Fluick\Flow;

use Fluick\Exception\FluickException;
use Fluick\Flow\Condition\Transition\AndCondition;
use Fluick\Flow\Condition\Transition\Condition;
use Fluick\Flow\Exception\ActionFailedException;
use Fluick\Flow\Exception\FlowException;

/**
 * Class Transition handles the transition from a step to another.
 *
 * @package Fluick\Flow
 */
class Transition extends Element
{
    /**
     * Actions which will be executed during the transition.
     *
     * @var Action[]
     */
    private $actions = [];

    /**
     * Post actions which will be executed when new step is reached.
     *
     * @var Action[]
     */
    private $postActions = [];

    /**
     * The step the transition is moving to.
     *
     * @var Step
     */
    private $stepTo;

    /**
     * A pre condition which has to be passed to execute transition.
     *
     * @var Condition
     */
    private $preCondition;

    /**
     * A condition which has to be passed to execute the transition.
     *
     * @var Condition
     */
    private $condition;

    /**
     * The corresponding definition.
     *
     * @var Definition
     */
    private $definition;

    /**
     * Transition constructor.
     *
     * @param string $name Name of the element.
     * @param Definition $definition The definition to which the transition belongs.
     * @param Step|null $stepTo The target step.
     * @param string $label Label of the element.
     * @param array $config Configuration values.
     */
    public function __construct(string $name, Definition $definition,
                                ?Step $stepTo, string $label = '', array $config = [])
    {
        parent::__construct($name, $label, $config);

        $this->definition = $definition;
        $this->stepTo = $stepTo;
    }

    /**
     * Get the definition.
     *
     * @return Definition
     */
    public function getDefinition(): Definition
    {
        return $this->definition;
    }

    /**
     * Add an action to the transition.
     *
     * @param Action $action The added action.
     *
     * @return $this
     */
    public function addAction(Action $action): self
    {
        $this->actions[] = $action;

        return $this;
    }

    /**
     * Get all actions.
     *
     * @return Action[]
     */
    public function getActions(): array
    {
        return $this->actions;
    }

    /**
     * Add an post action to the transition.
     *
     * @param Action $action The added action.
     *
     * @return $this
     */
    public function addPostAction(Action $action): self
    {
        $this->postActions[] = $action;

        return $this;
    }

    /**
     * Get all post actions.
     *
     * @return Action[]
     */
    public function getPostActions(): array
    {
        return $this->postActions;
    }

    /**
     * Get the target step.
     *
     * @return Step
     */
    public function getStepTo(): ?Step
    {
        return $this->stepTo;
    }

    /**
     * Get the condition.
     *
     * @return Condition|null
     */
    public function getCondition(): ?Condition
    {
        return $this->condition;
    }

    /**
     * Add a condition.
     *
     * @param Condition $condition The new condition.
     *
     * @return $this
     */
    public function addCondition(Condition $condition): self
    {
        if (!$this->condition) {
            $this->condition = new AndCondition();
        }
        $this->condition->addCondition($condition);

        return $this;
    }

    /**
     * Get the precondition.
     *
     * @return Condition
     */
    public function getPreCondition(): ?Condition
    {
        return $this->preCondition;
    }

    /**
     * Add a precondition precondition.
     *
     * @param Condition $preCondition The new precondition.
     *
     * @return $this
     */
    public function addPreCondition(Condition $preCondition): self
    {
        if (!$this->preCondition) {
            $this->preCondition = new AndCondition();
        }

        $this->preCondition->addCondition($preCondition);

        return $this;
    }


    /**
     * Consider if user input is required.
     *
     * @param Item $item Workflow item.
     *
     * @return array
     */
    public function getRequiredPayloadProperties(Item $item): array
    {
        if (empty($this->actions)) {
            return [];
        }

        return array_merge(
            ... array_map(
            static function (Action $action) use ($item) {
                return $action->getRequiredPayloadProperties($item);
            },
            $this->actions
        ),
            ... array_map(
                static function (Action $action) use ($item) {
                    return $action->getRequiredPayloadProperties($item);
                },
                $this->postActions
            )
        );
    }

    /**
     * Validate the given item and context (payload properties).
     *
     * @param Item $item Workflow item.
     * @param Context $context Transition context.
     *
     * @return bool
     */
    public function validate(Item $item, Context $context): bool
    {
        $validated = true;

        foreach ($this->actions as $action) {
            $validated = $validated && $action->validate($item, $context);
        }

        foreach ($this->postActions as $action) {
            $validated = $validated && $action->validate($item, $context);
        }

        return $validated;
    }

    /**
     * Execute the transition to the next state.
     *
     * @param Item $item Workflow item.
     * @param Context $context Transition context.
     *
     * @return State
     *
     * @throws FlowException When transition fails.
     * @throws FluickException
     */
    public function execute(Item $item, Context $context): State
    {
        $currentState = $item->getLatestStateOccurred();
        $success = $this->doExecuteActions($item, $context, $this->actions);

        if ($this->getStepTo()) {
            if ($item->isWorkflowStarted()) {
                $item->transit($this, $context, $success);
            } else {
                $item->start($this, $context, $success);
            }
        }

        $this->doExecuteActions($item, $context, $this->postActions);

        if ($this->getStepTo() === null && $currentState === $item->getLatestStateOccurred()) {
            throw new FlowException(
                sprintf('No state changes within the transition "%s"', $this->getName())
            );
        }

        return $item->getLatestStateOccurred();
    }

    /**
     * Consider if transition is allowed.
     *
     * @param Item $item The Item.
     * @param Context $context The transition context.
     *
     * @return bool
     */
    public function isAllowed(Item $item, Context $context): bool
    {
        if ($this->checkPreCondition($item, $context)) {
            return $this->checkCondition($item, $context);
        }

        return false;
    }

    /**
     * Consider if transition is available.
     *
     * If a transition can be available but it is not allowed depending on the user input.
     *
     * @param Item $item The Item.
     * @param Context $context The transition context.
     *
     * @return bool
     */
    public function isAvailable(Item $item, Context $context): bool
    {
        if ($this->getRequiredPayloadProperties($item)) {
            return $this->checkPreCondition($item, $context);
        }

        return $this->isAllowed($item, $context);
    }

    /**
     * Check the precondition.
     *
     * @param Item $item The Item.
     * @param Context $context The transition context.
     *
     * @return bool
     */
    public function checkPreCondition(Item $item, Context $context): bool
    {
        return $this->performConditionCheck($item, $context, $this->preCondition);
    }

    /**
     * Check the condition.
     *
     * @param Item $item The Item.
     * @param Context $context The transition context.
     *
     * @return bool
     */
    public function checkCondition(Item $item, Context $context): bool
    {
        return $this->performConditionCheck($item, $context, $this->condition);
    }

    /**
     * Perform condition check.
     *
     * @param Condition|null $condition Condition to be checked.
     * @param Item $item Workflow item.
     * @param Context $context Condition context.
     *
     * @return bool
     */
    private function performConditionCheck(Item $item, Context $context, ?Condition $condition = null): bool
    {
        if (!$condition) {
            return true;
        }

        return $condition->match($this, $item, $context);
    }

    /**
     * Execute the actions.
     *
     * @param Item $item Workflow item.
     * @param Context $context Condition context.
     * @param Action[] $actions Action to execute.
     *
     * @return bool
     */
    private function doExecuteActions(Item $item, Context $context, array $actions): bool
    {
        $success = $this->isAllowed($item, $context);

        if ($success) {
            try {
                foreach ($actions as $action) {
                    $action->transit($this, $item, $context);
                }
            } catch (ActionFailedException $e) {
                $params = [
                    'exception' => $e->getMessage(),
                    'action' => $e->actionName()
                ];
                $context->addError('transition.action.failed', $params, $e->errorCollection());

                return false;
            }
        }

        return $success && !$context->getErrorCollection()->hasErrors();
    }

}