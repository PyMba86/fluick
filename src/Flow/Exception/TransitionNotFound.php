<?php

namespace Fluick\Flow\Exception;

use Exception;

/**
 * Class TransitionNotFoundException is thrown then transition was not found.
 *
 */
class TransitionNotFound extends FlowException
{
    /**
     * Construct.
     *
     * @param string $transitionName The not found transition name.
     * @param string $workflowName Current workflow name.
     * @param int $code Error code.
     * @param Exception|null $previous Previous thrown exception.
     *
     * @return TransitionNotFound
     */
    public static function withName(
        string $transitionName,
        string $workflowName,
        int $code = 0,
        Exception $previous = null
    ): TransitionNotFound
    {
        return new self(
            sprintf('Transition "%s" not found in workflow "%s"', $transitionName, $workflowName),
            $code,
            $previous
        );
    }
}