<?php

namespace Fluick\Flow\Exception;

use Fluick\Flow\Action;
use Fluick\Flow\Context\ErrorCollection;
use Fluick\Flow\Element;

/**
 * Class TransactionActionFailed is thrown then a transaction action failed.
 *
 * @package Fluick\Flow\Exception
 */
class ActionFailedException extends FlowException
{
    /**
     * The action name.
     *
     * @var string|null
     */
    private $actionName;

    /**
     * Additional error collection.
     *
     * @var ErrorCollection|null
     */
    private $errorCollection;

    /**
     * Create exception for with an action name.
     *
     * @param string               $actionName      The action name.
     * @param ErrorCollection|null $errorCollection Additional error collection.
     *
     * @return ActionFailedException
     */
    public static function namedAction(string $actionName, ?ErrorCollection $errorCollection = null): self
    {
        $exception                  = new self(sprintf('Execution of action "%s" failed.', $actionName));
        $exception->actionName      = $actionName;
        $exception->errorCollection = $errorCollection;

        return $exception;
    }

    /**
     * Create exception for an action.
     *
     * @param Action               $action          The action.
     * @param ErrorCollection|null $errorCollection Additional error collection.
     *
     * @return ActionFailedException
     */
    public static function action(Action $action, ?ErrorCollection $errorCollection = null): self
    {
        if ($action instanceof Element) {
            $actionName = $action->getDescription();
        } else {
            $parts      = explode('\\', trim(get_class($action), '\\'));
            $actionName = end($parts);
        }

        return self::namedAction($actionName, $errorCollection);
    }

    /**
     * Get the action name.
     *
     * @return string|null
     */
    public function actionName(): ?string
    {
        return $this->actionName;
    }

    /**
     * Get the error collection.
     *
     * @return ErrorCollection|null
     */
    public function errorCollection(): ?ErrorCollection
    {
        return $this->errorCollection;
    }
}