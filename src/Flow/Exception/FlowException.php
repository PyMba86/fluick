<?php

namespace Fluick\Flow\Exception;

use Fluick\Exception\FluickException;
use RuntimeException;

/**
 * Class FlowException
 *
 * @package Fluick\Flow\Exception
 */
class FlowException extends RuntimeException implements FluickException
{

}