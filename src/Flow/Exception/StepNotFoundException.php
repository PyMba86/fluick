<?php

namespace Fluick\Flow\Exception;

use Exception;

class StepNotFoundException extends FlowException
{
    /**
     * Construct.
     *
     * @param string $stepName The step name which is not found.
     * @param string $workflowName Current workflow name.
     * @param int $code Error code.
     * @param Exception|null $previous Previous thrown exception.
     */
    public function __construct(string $stepName, string $workflowName, int $code = 0, Exception $previous = null)
    {
        parent::__construct(
            sprintf('Step "%s" is not part of workflow "%s"', $stepName, $workflowName),
            $code,
            $previous
        );
    }
}