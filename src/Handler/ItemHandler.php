<?php

namespace Fluick\Handler;

use Fluick\Flow\Item;
use Fluick\Flow\Workflow;

interface ItemHandler
{

    public function handle(int $entityId): Item;
}