<?php

namespace Fluick\Handler;

use Fluick\Flow\Item;
use Fluick\Repository\StateRepository;

class RepositoryItemHandler implements ItemHandler
{
    /**
     * The entity
     *
     * @var array
     */
    protected $entity;

    /**
     * The state repository.
     *
     * @var StateRepository
     */
    protected $stateRepository;

    /**
     * @param array $entity
     * @param StateRepository $stateRepository
     */
    public function __construct(array $entity, StateRepository $stateRepository)
    {
        $this->entity = $entity;
        $this->stateRepository = $stateRepository;
    }

    /**
     * @param int $entityId
     * @return Item
     */
    public function handle(int $entityId): Item
    {
        $stateHistory = $this->stateRepository
            ->find($entityId);

        return Item::reconstitute($entityId, $this->entity, $stateHistory);
    }
}