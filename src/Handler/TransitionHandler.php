<?php

namespace Fluick\Handler;

use Exception;
use Fluick\Exception\FluickException;
use Fluick\Flow\Context;
use Fluick\Flow\Definition;
use Fluick\Flow\Item;
use Fluick\Flow\State;
use Fluick\Flow\Step;
use Fluick\Flow\Transition;

interface TransitionHandler
{
    /**
     * Get the definition
     *
     * @return Definition
     */
    public function getDefinition(): Definition;

    /**
     * Get the item
     *
     * @return Item
     */
    public function getItem(): Item;

    /**
     * Get the transition.
     *
     * @return Transition
     */
    public function getTransition(): Transition;

    /**
     * Get current step. Will return null if workflow is not started yet.
     *
     * @return Step|null
     */
    public function getCurrentStep(): ?Step;

    /**
     * Consider if it handles a start transition.
     *
     * @return bool
     */
    public function isWorkflowStarted(): bool;

    /**
     * Consider if transition is available.
     *
     * @return bool
     */
    public function isAvailable(): bool;

    /**
     * Get the context.
     *
     * @return Context
     */
    public function getContext(): Context;

    /**
     * Handle to next step.
     *
     * @param array $payload The payload.
     * @return State
     * @throws FluickException For a workflow specific error.
     * @throws Exception For any error caused maybe by 3rd party code in the actions.
     */
    public function handle(array $payload = []): State;
}