<?php

namespace Fluick\Handler;

use Fluick\Flow\Definition;
use Fluick\Flow\Item;

interface WorkflowHandler
{
    /**
     * Create transition handler
     *
     * @param Item $item
     * @param Definition $definition
     * @param string|null $transitionName
     * @return TransitionHandler
     */
    public function createTransition(Item $item, Definition $definition,
                                     string $transitionName = null): TransitionHandler;

    /**
     * Create item handler
     *
     * @param array $entity
     * @return ItemHandler
     */
    public function createItem(array $entity): ItemHandler;
}