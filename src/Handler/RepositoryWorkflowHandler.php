<?php

namespace Fluick\Handler;

use Fluick\Flow\Definition;
use Fluick\Flow\Item;
use Fluick\Repository\EntityRepository;
use Fluick\Repository\StateRepository;
use Fluick\Transaction\TransactionHandler;

class RepositoryWorkflowHandler implements WorkflowHandler
{
    /**
     * The entity repository.
     *
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * The state repository.
     *
     * @var StateRepository
     */
    protected $stateRepository;

    /**
     * @var TransactionHandler
     */
    protected $transactionHandler;

    /**
     * RepositoryHandlerFactory constructor.
     * @param EntityRepository $entityRepository
     * @param StateRepository $stateRepository
     * @param TransactionHandler $transactionHandler
     */
    public function __construct(EntityRepository $entityRepository,
                                StateRepository $stateRepository,
                                TransactionHandler $transactionHandler)
    {
        $this->entityRepository = $entityRepository;
        $this->stateRepository = $stateRepository;
        $this->transactionHandler = $transactionHandler;
    }

    /**
     * @inheritdoc
     */
    public function createTransition(Item $item, Definition $definition,
                                     string $transitionName = null): TransitionHandler
    {
        return new RepositoryTransitionHandler($item, $definition,
            $this->entityRepository, $this->stateRepository,
            $this->transactionHandler, $transitionName);
    }

    /**
     * @inheritdoc
     */
    public function createItem(array $entity): ItemHandler
    {
        return new RepositoryItemHandler($entity, $this->stateRepository);
    }
}