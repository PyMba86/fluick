<?php

namespace Fluick\Handler;

use Exception;
use Fluick\Flow\Definition;
use Fluick\Flow\Item;
use Fluick\Flow\State;
use Fluick\Repository\EntityRepository;
use Fluick\Repository\StateRepository;
use Fluick\Transaction\TransactionHandler;

/**
 *  Class RepositoryTransitionHandler handles the transition to another step in the workflow.
 *
 * @package Fluick\Handler
 */
class RepositoryTransitionHandler extends AbstractTransitionHandler
{
    /**
     * The entity repository.
     *
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * The state repository.
     *
     * @var StateRepository
     */
    protected $stateRepository;

    /**
     * @param Item $item
     * @param Definition $definition
     * @param string|null $transitionName
     * @param EntityRepository $entityRepository
     * @param StateRepository $stateRepository
     * @param TransactionHandler $transactionHandler
     */
    public function __construct(
        Item $item,
        Definition $definition,
        EntityRepository $entityRepository,
        StateRepository $stateRepository,
        TransactionHandler $transactionHandler,
        string $transitionName = null
    )
    {
        parent::__construct($item, $definition, $transitionName, $transactionHandler);

        $this->entityRepository = $entityRepository;
        $this->stateRepository = $stateRepository;
    }

    public function handle(array $payload = []): State
    {
        $this->guardValidated($payload);

        $this->transactionHandler->begin();

        try {
            $state = $this->executeTransition();

            foreach ($this->getItem()->releaseRecordedStateChanges() as $state) {
                $this->stateRepository->add($state);
            }

            $item = $this->getItem();

            $this->entityRepository->update($item);

        } catch (Exception $e) {
            $this->transactionHandler->rollback();

            throw $e;
        }

        $this->transactionHandler->commit();

        return $state;
    }
}