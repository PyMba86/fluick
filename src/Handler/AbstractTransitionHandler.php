<?php

namespace Fluick\Handler;

use Fluick\Exception\FluickException;
use Fluick\Flow\Context;
use Fluick\Flow\Definition;
use Fluick\Flow\Exception\FlowException;
use Fluick\Flow\Item;
use Fluick\Flow\State;
use Fluick\Flow\Step;
use Fluick\Flow\Transition;
use Fluick\Transaction\TransactionHandler;

/**
 * AbstractTransitionHandler can be used as base class for transition handler implementations.
 *
 * @package Fluick\Handler
 */
abstract class AbstractTransitionHandler implements TransitionHandler
{
    /**
     * The given entity.
     *
     * @var Item
     */
    private $item;

    /**
     * The current definition
     *
     * @var Definition
     */
    private $definition;

    /**
     * The transition name which will be handled.
     *
     * @var string
     */
    private $transitionName;

    /**
     * The transaction handler.
     *
     * @var TransactionHandler
     */
    protected $transactionHandler;

    /**
     * The transition context.
     *
     * @var Context
     */
    private $context;

    /**
     * @param Item $item
     * @param Definition $definition
     * @param string $transitionName
     * @param TransactionHandler $transactionHandler
     */
    public function __construct(
        Item $item,
        Definition $definition,
        string $transitionName,
        TransactionHandler $transactionHandler
    )
    {
        $this->item = $item;
        $this->definition = $definition;
        $this->transitionName = $transitionName;
        $this->transactionHandler = $transactionHandler;
        $this->context = new Context();

        $this->guardAllowedTransition($transitionName);
    }

    /**
     * {@inheritdoc}
     */
    public function getTransition(): Transition
    {
        if ($this->isWorkflowStarted()) {
            return $this->definition->getTransition($this->transitionName);
        }

        return $this->definition->getStartTransition();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefinition(): Definition
    {
        return $this->definition;
    }

    /**
     * {@inheritdoc}
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * {@inheritdoc}
     */
    public function getContext(): Context
    {
        return $this->context;
    }

    /**
     * {@inheritdoc}
     */
    public function isWorkflowStarted(): bool
    {
        return $this->item->isWorkflowStarted();
    }

    /**
     * Consider if transition is available.
     *
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->getTransition()
            ->isAvailable($this->item, $this->context);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentStep(): ?Step
    {
        if ($this->isWorkflowStarted()) {
            $stepName = $this->item->getCurrentStepName();

            return $this->definition->getStep($stepName);
        }

        return null;
    }

    protected function validate(array $payload = []): bool
    {
        // first build the form
        $this->context = $this->context->createCleanCopy($payload);
        $validated = false;
        $transition = $this->getTransition();

        // check pre conditions first
        if ($transition->checkPreCondition($this->item, $this->context)) {
            $validated = true;
        }

        // Validate the actions
        if (!$transition->validate($this->item, $this->context)) {
            $validated = false;
        }

        if ($validated && !$transition->checkCondition($this->item, $this->context)) {
            $validated = false;
        }

        return $validated;
    }

    /**
     * Execute the transition.
     *
     * @return State
     * @throws FluickException
     */
    protected function executeTransition(): State
    {
        return $this->getTransition()->execute($this->item, $this->context);
    }

    /**
     * Guard that transition was validated before.
     *
     * @param array $payload
     * @return void
     */
    protected function guardValidated(array $payload = []): void
    {
        $validate = $this->validate($payload);

        if ($validate === null) {
            throw new FlowException('Transition was not validated so far.');
        } elseif (!$validate) {
            throw new FlowException('Transition is in a invalid state and can\'t be processed.');
        }
    }

    /**
     * Guard that requested transition is allowed.
     *
     * @param string|null $transitionName Transition to be processed.
     *
     * @return void
     * @throws FlowException If Transition is not allowed.
     *
     */
    private function guardAllowedTransition(?string $transitionName): void
    {
        if (!$this->isWorkflowStarted()) {
            if ($transitionName === null || $transitionName === $this->getDefinition()
                    ->getStartTransition()->getName()) {
                return;
            }

            throw new FlowException(
                sprintf(
                    'Not allowed to process transition "%s". Workflow "%s" not started for item "%s"',
                    $transitionName,
                    $this->definition->getName(),
                    $this->item->getEntityId()
                )
            );
        }

        $step = $this->getCurrentStep();

        if (!$step->isTransitionAllowed($transitionName)) {
            throw new FlowException(
                sprintf(
                    'Not allowed to process transition "%s". Transition is not allowed in step "%s"',
                    $transitionName,
                    $step->getName()
                )
            );
        }
    }
}