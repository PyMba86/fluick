<?php

namespace Fluick\Process;

use Fluick\Flow\Item;
use Fluick\Handler\ItemHandler;
use Fluick\Handler\TransitionHandler;

/**
 * Interface Process
 *
 * @package Fluick\Process
 */
interface Process
{
    /**
     * Handle transition workflow process
     *
     * @param Item $item
     * @param string|null $transitionName
     * @return TransitionHandler
     */
    public function transition(Item $item, string $transitionName = null): TransitionHandler;

    /**
     * Handle item workflow process
     *
     * @param array $entity
     * @return ItemHandler
     */
    public function item(array $entity): ItemHandler;
}