<?php

namespace Fluick\Process;

use Fluick\Flow\Definition;
use Fluick\Flow\Exception\FlowException;
use Fluick\Flow\Item;
use Fluick\Handler\WorkflowHandler;
use Fluick\Handler\ItemHandler;
use Fluick\Handler\TransitionHandler;

class WorkflowProcess implements Process
{
    /**
     * @var Definition
     */
    protected $definition;

    /**
     * @var WorkflowHandler
     */
    protected $handler;

    /**
     * @param Definition $definition
     * @param WorkflowHandler $handler
     */
    public function __construct(Definition $definition, WorkflowHandler $handler)
    {
        $this->definition = $definition;
        $this->handler = $handler;
    }

    /**
     * @inheritdoc
     */
    public function transition(Item $item, string $transitionName = null,
                               bool $changeWorkflow = false): TransitionHandler
    {
        $changed = $this->hasWorkflowChanged($item, !$changeWorkflow);

        if ($changed && $changeWorkflow) {
            $item->detach();
        }

        return $this->handler->createTransition(
            $item, $this->definition, $transitionName);
    }

    /**
     * @inheritdoc
     */
    public function item(array $entity): ItemHandler
    {
        return $this->handler->createItem($entity);
    }

    /**
     * Guard that already started workflow is the same which is tried to be ran now.
     *
     * @param Item $item Current workflow item.
     * @param bool $throw If true an error is thrown.
     *
     * @return bool
     * @throws FlowException If item workflow is not the same as current workflow.
     *
     */
    private function hasWorkflowChanged(Item $item, bool $throw = true): bool
    {
        $workflowName = $this->definition->getName();

        if ($item->isWorkflowStarted() && $item->getWorkflowName() != $workflowName) {
            $message = sprintf(
                'Item "%s" already process workflow "%s" and cannot be handled by "%s"',
                $item->getEntityId(),
                $item->getWorkflowName(),
                $workflowName
            );

            if ($throw) {
                throw new FlowException($message);
            }

            return true;
        }

        return false;
    }

}