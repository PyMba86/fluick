<?php

namespace Fluick\Exception;

/**
 * Class WorkflowException is thrown if something went wrong during workflow.
 *
 * @package Fluick\Exception
 */
interface FluickException
{

}