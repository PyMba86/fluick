<?php

namespace Fluick\Transaction;

/**
 * Class DelegatingTransactionHandler delegates transaction commands to its children handlers.
 *
 * @package Fluick\Transaction
 */
class DelegatingTransactionHandler implements TransactionHandler
{
    /**
     * Transaction handler.
     *
     * @var TransactionHandler[]
     */
    private $transactionHandlers;

    /**
     * DelegatingTransactionHandler constructor.
     *
     * @param TransactionHandler[] $transactionHandlers Child transaction handlers.
     */
    public function __construct(array $transactionHandlers)
    {
        $this->transactionHandlers = $transactionHandlers;
    }

    /**
     * {@inheritdoc}
     */
    public function begin(): void
    {
        foreach ($this->transactionHandlers as $handler) {
            $handler->begin();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function commit(): void
    {
        foreach ($this->transactionHandlers as $handler) {
            $handler->commit();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rollback(): void
    {
        foreach ($this->transactionHandlers as $handler) {
            $handler->rollback();
        }
    }
}