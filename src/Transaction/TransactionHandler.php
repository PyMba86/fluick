<?php

namespace Fluick\Transaction;

/**
 * Interface TransactionHandler describes the commonly used transaction steps begin, commit and rollback.
 *
 * @package Fluick\Transaction
 */
interface TransactionHandler
{
    /**
     * Begin a transaction.
     *
     * @return void
     */
    public function begin(): void;

    /**
     * Commit changes.
     *
     * @return void
     */
    public function commit(): void;

    /**
     * Rollback the transaction.
     *
     * @return void
     */
    public function rollback(): void;
}